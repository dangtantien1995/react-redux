import React from 'react';

class Note extends React.Component{
  render(){
    return (
      <div>
        <p>{this.props.children}</p>
        <button className="btn btn-danger" onClick={this.removeNote.bind(this)}>Delete</button>
      </div>
    )
  }
  removeNote(){
    var {index, handleRemove} = this.props;
    handleRemove(index);
  }
}

module.exports = Note;
