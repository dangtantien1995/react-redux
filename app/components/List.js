import React from 'react';
import Note from './Note';
import NoteForm from './NoteForm';

class Hello extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      mang : ['Item 1', 'Item 2', 'Item 3']
    }
  }

  render(){
    return(
      <div className="container">
        <NoteForm handleAdd={this.addNote.bind(this)}>

        </NoteForm>
          {this.state.mang.map((e ,i) => <Note index={i} handleRemove={this.remove.bind(this)} key={i}>{e}</Note>)}
      </div>
    )
  }

  remove(index){
    this.state.mang.splice(index , 1);
    this.setState(this.state);
  }

  addNote(note){
    this.state.mang.push(note);
    this.setState(this.state);
  }
}

module.exports = Hello;
