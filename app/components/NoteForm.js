import React from 'react';

class NoteForm extends React.Component{
  render(){
    return(
      <div>
        <br/>
        <div className="">
          Add Somethings here
        </div>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <div>
            <input className="form-control myinput" type="text" placeholder="Enter somethings" ref="txt"/>
          </div>
          <div>
            <button type="submit" className="btn btn-primary">Add</button>
          </div>
          <br/>
          <br/>
        </form>
      </div>
    )
  }
  handleSubmit(e){
    e.preventDefault(); //khi nhấn enter thì sẽ ko bị chuyển trang vì là form
    this.props.handleAdd(this.refs.txt.value);
    this.refs.txt.value = "";
  }
}

module.exports = NoteForm;
